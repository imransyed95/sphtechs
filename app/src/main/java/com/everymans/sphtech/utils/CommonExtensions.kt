package com.everymans.sphtech.utils

import android.content.Context
import android.widget.Toast
import com.everymans.sphtech.model.UsageData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.*


fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


fun extrack4CharacterFromString(data:String):String{
    return data.take(4)
}

fun isSorted(array: ArrayList<Double>): Boolean {
    for (i in 0 until array.size - 1) {
        if (array[i] > array[i + 1])
            return false
    }
    return true
}

fun write(context: Context,usageList: java.util.ArrayList<UsageData>) {
    val json = Gson().toJson(usageList)
    try {
        val cacheDir = context.cacheDir.toString()
        val out: ObjectOutput =
            ObjectOutputStream(FileOutputStream(File(cacheDir + File.separator + "cacheFile.srl")))
        out.writeObject(json)
        out.close()
    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    } catch (e: IOException) {
        e.printStackTrace()
    }

}


 fun read(context: Context):ArrayList<UsageData> {
    var dat: String? = null
    var cacheList=ArrayList<UsageData>()
    try {
        val `in` =
            ObjectInputStream(FileInputStream(
                File(context.cacheDir.toString() + File.separator + "cacheFile.srl")))
        dat = `in`.readObject() as String
        `in`.close()


        cacheList= Gson().fromJson(dat, object : TypeToken<java.util.ArrayList<UsageData>>() {

        }.type)


    } catch (e: ClassNotFoundException) {
        e.printStackTrace()
    } catch (e: OptionalDataException) {
        e.printStackTrace()
    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    } catch (e: StreamCorruptedException) {
        e.printStackTrace()
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return cacheList
}

