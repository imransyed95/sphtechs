package com.everymans.sphtech.app

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.everymans.sphtech.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


open class MainApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        initiateKoin()
    }

    private fun initiateKoin() {
        startKoin{
            androidContext(this@MainApp)
            modules(provideDependency())
        }
    }

    open fun provideDependency() = appComponent

    companion object {
        lateinit  var appContext: Context
        var instance: MainApp = MainApp()
    }

}