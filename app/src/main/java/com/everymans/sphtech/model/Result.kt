package com.everymans.sphtech.model

import com.everymans.sphtech.utils.extrack4CharacterFromString
import com.everymans.sphtech.utils.isSorted
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Result {

    @SerializedName("resource_id")
    @Expose
    var resourceId: String? = null
    @SerializedName("fields")
    @Expose
    var fields: List<Field>? = null
    @SerializedName("records")
    @Expose
    var records: List<Record>? = null
    @SerializedName("_links")
    @Expose
    var links: Links? = null
    @SerializedName("limit")
    @Expose
    var limit: Int? = null
    @SerializedName("total")
    @Expose
    var total: Int? = null

    var listData = ArrayList<UsageData>()

    fun modifyRemoteData(): ArrayList<UsageData> {
        var count = 0
        var earluValumeData = 0.0
        val sequenceCheckList = ArrayList<Double>()
        records?.forEach {
            if (extrack4CharacterFromString(it.quarter!!).toInt() >= 2008 && extrack4CharacterFromString(it.quarter!!).toInt() <= 2018) {
                count = count + 1
                sequenceCheckList.add(it.volumeOfMobileData!!.toDouble())
                earluValumeData = earluValumeData.plus(it.volumeOfMobileData!!.toDouble())
                if (count == 4) {
                    val model = UsageData()
                    model.year = it.quarter!!.take(4)
                    model.yearlyData = earluValumeData.toString()
                    model.decreaseInSequence = isSorted(sequenceCheckList)
                    listData.add(model)
                    sequenceCheckList.clear()
                    count = 0
                    earluValumeData = 0.0
                }
            }
        }

        return listData
    }
}