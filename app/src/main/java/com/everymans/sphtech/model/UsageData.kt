package com.everymans.sphtech.model

class UsageData {
    var yearlyData: String? = null
    var year: String? = null
    var decreaseInSequence: Boolean = false
}