package com.everymans.sphtech.repository


import com.everymans.sphtech.model.MobileNetworkUsageRespone
import com.everymans.sphtech.network.UsageAPIService
import org.koin.core.KoinComponent
import org.koin.core.inject

class UsageRepository : KoinComponent {

    val mUsageAPIService: UsageAPIService by inject()

    suspend fun getLoginData(query: String): MobileNetworkUsageRespone {

        return processDataFetchUsage(query)

    }

    suspend fun processDataFetchUsage(parameter:String): MobileNetworkUsageRespone{
        val dataReceived = mUsageAPIService.getMobileNetworkUsageLists(parameter)
        return dataReceived
    }


}