package com.everymans.sphtech.network

import com.everymans.sphtech.model.MobileNetworkUsageRespone
import retrofit2.http.GET
import retrofit2.http.Query

interface UsageAPIService{

    @GET("api/action/datastore_search")
    suspend fun getMobileNetworkUsageLists(@Query("resource_id") resourseId: String): MobileNetworkUsageRespone
}