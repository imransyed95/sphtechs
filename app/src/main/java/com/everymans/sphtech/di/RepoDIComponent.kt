package com.everymans.sphtech.di


import com.everymans.sphtech.repository.UsageRepository
import org.koin.dsl.module


val RepoDependency = module {

    factory {
        UsageRepository()
    }

}