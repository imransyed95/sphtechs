package com.everymans.sphtech.di

import com.everymans.sphtech.ui.UsageUseCase
import org.koin.dsl.module


val UseCaseDependency = module {

    factory {
        UsageUseCase()
    }
}