package com.everymans.sphtech.platform

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.everymans.sphtech.utils.NetworkUtils
import com.everymans.sphtech.utils.Singleton

abstract class BaseActivity : AppCompatActivity(){

    var networkChangeReceiver: NetworkChangeReceiver? = null
    private var isNoNetAlertVisible = false
    inner class NetworkChangeReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            showNetworkError()
        }
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkChangeReceiver)
    }

    override fun onResume() {
        super.onResume()

        networkChangeReceiver = NetworkChangeReceiver()
        registerReceiver(networkChangeReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    fun showNetworkError(): Boolean {
        // Show popup when network is no available
        val isNetwork = NetworkUtils.isNetworkConnected(this)
        Singleton.isNetworkConnected = NetworkUtils.isNetworkConnected(this)
        if (!isNoNetAlertVisible && !NetworkUtils.isNetworkConnected(this)) {
            isNoNetAlertVisible = true
            putToast( "No internet connection or network failure")
        }
        return isNetwork
    }

    fun putToast(message: String?) {
        Toast.makeText(applicationContext, "" + message, Toast.LENGTH_SHORT).show()
    }
}