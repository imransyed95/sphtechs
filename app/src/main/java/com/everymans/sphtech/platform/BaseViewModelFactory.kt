package com.everymans.sphtech.platform

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.everymans.sphtech.ui.UsageActivityViewModel
import com.everymans.sphtech.ui.UsageUseCase
import kotlinx.coroutines.CoroutineDispatcher


class BaseViewModelFactory constructor(
    private val application: Application,
    private val mainDispather: CoroutineDispatcher
    ,private val ioDispatcher: CoroutineDispatcher
    ,private val useCase: UsageUseCase
) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(UsageActivityViewModel::class.java)) {
            UsageActivityViewModel(application,mainDispather, ioDispatcher,useCase) as T
        } else {
            throw IllegalArgumentException("ViewModel Not configured")
        }
    }
}