package com.everymans.sphtech.platform

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.everymans.sphtech.utils.NetworkUtils
import com.everymans.sphtech.utils.toast


abstract class BaseViewModel<N>(application: Application) : AndroidViewModel(application) {

    private val mApplication: Application = application
    private var mNavigator: N? = null

    fun isNetworkConnected(): Boolean {
        val network = NetworkUtils.isNetworkConnected(mApplication)
        return network
    }

     fun putToast(msg: String) {
        mApplication.toast(msg)
    }

    fun getNavigator(): N {
        return mNavigator!!
    }

    fun setNavigator(navigator: N) {
        this.mNavigator = navigator
    }
}