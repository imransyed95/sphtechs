package com.everymans.sphtech.ui

import com.everymans.sphtech.model.MobileNetworkUsageRespone
import com.everymans.sphtech.repository.UsageRepository
import org.koin.core.KoinComponent
import org.koin.core.inject


class UsageUseCase : KoinComponent {

    val mUsageRepo : UsageRepository by inject()

    suspend fun processUsageUseCase(query: String) : MobileNetworkUsageRespone {

        val response =  mUsageRepo.getLoginData(query)

        return response
    }
}