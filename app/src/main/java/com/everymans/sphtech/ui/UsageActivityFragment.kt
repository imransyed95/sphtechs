package com.everymans.sphtech.ui


import android.app.Application
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.everymans.sphtech.BR
import com.everymans.sphtech.R
import com.everymans.sphtech.adapter.BaseRecyclerViewAdapter
import com.everymans.sphtech.adapter.OnDataBindCallback
import com.everymans.sphtech.app.MainApp
import com.everymans.sphtech.constants.RESOURCE_ID
import com.everymans.sphtech.databinding.UsageListItemBinding
import com.everymans.sphtech.model.MobileNetworkUsageRespone
import com.everymans.sphtech.model.UsageData
import com.everymans.sphtech.platform.BaseFragment
import com.everymans.sphtech.platform.BaseViewModelFactory
import com.everymans.sphtech.platform.LiveDataWrapper
import com.everymans.sphtech.utils.read
import com.everymans.sphtech.utils.write
import kotlinx.android.synthetic.main.fragment_main_activity.*
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.android.inject

class UsageActivityFragment : BaseFragment(), OnDataBindCallback<UsageListItemBinding> {

    private var baseRecyclerAdapter: BaseRecyclerViewAdapter<UsageData, UsageListItemBinding>? =
        null

    var  listItems=ArrayList<UsageData>()

    val mLoginUseCase : UsageUseCase by inject()

    private var mBaseViewModelFactory : BaseViewModelFactory? =null


    private val TAG = UsageActivityFragment::class.java.simpleName

    //lateinit var mRecyclerViewAdapter: UsageRecyclerViewAdapter

    private val mViewModel : UsageActivityViewModel by lazy {
        ViewModelProviders.of(this,
            mBaseViewModelFactory)
            .get(UsageActivityViewModel::class.java)    }


    override fun getLayoutId(): Int = R.layout.fragment_main_activity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mBaseViewModelFactory=BaseViewModelFactory(context!!.applicationContext as Application,Dispatchers.Main, Dispatchers.IO,mLoginUseCase)
        this.mViewModel.mAllPeopleResponse.observe(this, this.mDataObserver)
        this.mViewModel.mLoadingLiveData.observe(this, this.loadingObserver)
        this.mViewModel.requestLoginActivityData(RESOURCE_ID)
    }

    //---------------Observers---------------//
    private val mDataObserver = Observer<LiveDataWrapper<MobileNetworkUsageRespone>> { result ->
        when (result?.responseRESPONSESTATUS) {
            LiveDataWrapper.RESPONSESTATUS.LOADING -> {
                // Loading data
            }
            LiveDataWrapper.RESPONSESTATUS.ERROR -> {
                logD(TAG,"LiveDataResult.Status.ERROR = ${result.response}")
                when {
                    read(context!!).isEmpty() -> {
                        error_holder.visibility = View.VISIBLE
                        putToast("Error in getting data")
                    }
                    else -> {
                        setUpRecyclerview(read(context!!))
                        error_holder.visibility = View.GONE
                        putToast("Data fetched from chache")
                    }
                }
            }
            LiveDataWrapper.RESPONSESTATUS.SUCCESS -> {
                logD(TAG,"LiveDataResult.Status.SUCCESS = ${result.response}")
                val mainItemReceived = result.response as MobileNetworkUsageRespone
                listItems =  mainItemReceived.result?.modifyRemoteData() as ArrayList<UsageData>
                write(context!!,listItems)
                setUpRecyclerview(listItems)
               // processData(listItems)
            }
        }
    }


    private fun setUpRecyclerview(usageList: java.util.ArrayList<UsageData>) {
        baseRecyclerAdapter = BaseRecyclerViewAdapter(
            R.layout.usage_list_item,
            BR.usageItem,
            usageList,
            null,
            this
        )

        landingListRecyclerView.adapter = baseRecyclerAdapter
        baseRecyclerAdapter?.notifyDataSetChanged()
    }


   /* private fun processData(listItems: ArrayList<UsageData>) {
        logD(TAG,"processData called with data ${listItems.size}")
        logD(TAG,"processData listItems =  ${listItems}")

        val refresh = Handler(Looper.getMainLooper())
        refresh.post {
            mRecyclerViewAdapter.updateListItems(listItems)
        }
    }*/

    /**
     * Hide / show loader
     */
    private val loadingObserver = Observer<Boolean> { visible ->
        if(visible){
            progress_circular.visibility = View.VISIBLE
        }else{
            progress_circular.visibility = View.INVISIBLE
        }
    }


    override fun onItemClick(view: View, position: Int, v: UsageListItemBinding) {
        if (view == v.decreaseIv) {
            putToast("The Year " + listItems[position].year + " has downgrade in Volume of data sent over")
        }
    }

    override fun onDataBind(v: UsageListItemBinding, onClickListener: View.OnClickListener) {
        v.decreaseIv.setOnClickListener(onClickListener)
    }

    companion object{
        fun getMainActivityFragment() = UsageActivityFragment()
    }

}
