package com.everymans.sphtech.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.everymans.sphtech.model.MobileNetworkUsageRespone
import com.everymans.sphtech.platform.BaseViewModel
import com.everymans.sphtech.platform.LiveDataWrapper
import com.everymans.sphtech.platform.BaseNavigator
import kotlinx.coroutines.*
import org.koin.core.KoinComponent


class UsageActivityViewModel(
    application: Application,
    mainDispatcher: CoroutineDispatcher,
    ioDispatcher: CoroutineDispatcher,
    private val useCase: UsageUseCase
) : BaseViewModel<BaseNavigator>(application), KoinComponent {

    var mAllPeopleResponse = MutableLiveData<LiveDataWrapper<MobileNetworkUsageRespone>>()
    val mLoadingLiveData = MutableLiveData<Boolean>()
    val mNetworkAvailable = MutableLiveData<Boolean>()
    private val job = SupervisorJob()
    val mUiScope = CoroutineScope(mainDispatcher + job)
    val mIoScope = CoroutineScope(ioDispatcher + job)


    fun requestLoginActivityData(param: String) {
        if (mAllPeopleResponse.value == null) {
            mUiScope.launch {
                mAllPeopleResponse.value = LiveDataWrapper.loading()
                setLoadingVisibility(true)
                try {
                    val data = mIoScope.async {
                        return@async useCase.processUsageUseCase(param)
                    }.await()
                    try {
                        mAllPeopleResponse.value = LiveDataWrapper.success(data)
                    } catch (e: Exception) {
                        Log.d("eee", "" + e)
                    }
                    setLoadingVisibility(false)
                } catch (e: Exception) {
                    setLoadingVisibility(false)
                    mAllPeopleResponse.value = LiveDataWrapper.error(e)
                }
            }
        }
    }

    private fun setLoadingVisibility(visible: Boolean) {
        mLoadingLiveData.postValue(visible)
    }

    override fun onCleared() {
        super.onCleared()
        this.job.cancel()
    }
}