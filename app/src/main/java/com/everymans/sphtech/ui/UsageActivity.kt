package com.everymans.sphtech.ui

import android.os.Bundle
import com.everymans.sphtech.R
import com.everymans.sphtech.platform.BaseActivity


class UsageActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureAndShowFragment()
    }

    private fun configureAndShowFragment() {
        var fragment = supportFragmentManager.findFragmentById(R.id.base_frame_layout) as UsageActivityFragment?
        if(fragment == null){
            supportFragmentManager.beginTransaction()
                .add(R.id.base_frame_layout, UsageActivityFragment.getMainActivityFragment())
                .commit()
        }
    }
}
