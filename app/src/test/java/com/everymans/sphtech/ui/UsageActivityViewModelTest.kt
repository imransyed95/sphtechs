package com.everymans.sphtech.ui

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.everymans.sphtech.app.MainApp
import com.everymans.sphtech.di.configureTestAppComponent
import com.everymans.sphtech.base.BaseUTTest
import com.everymans.sphtech.model.MobileNetworkUsageRespone
import com.everymans.sphtech.platform.LiveDataWrapper
import com.google.gson.Gson
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class UsageActivityViewModelTest: BaseUTTest(){

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    lateinit var mUsageActivityViewModel: UsageActivityViewModel

    val mDispatcher = Dispatchers.Unconfined
    var context = mock(Application::class.java)
    @MockK
    lateinit var mUsageUseCase: UsageUseCase

    val mTotalValue = "59"
    val mResourceId = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"

    @Before
    fun start(){
        super.setUp()
        MockKAnnotations.init(this)
        startKoin{ modules(configureTestAppComponent(getMockWebServerUrl()))}
    }

    @Test
    fun test_login_view_model_data_populates_expected_value(){
        mUsageActivityViewModel = UsageActivityViewModel(context,mDispatcher,mDispatcher,mUsageUseCase)
        val sampleResponse = getJson("success_resp_list.json")
        val jsonObj = Gson().fromJson(sampleResponse, MobileNetworkUsageRespone::class.java)
        coEvery { mUsageUseCase.processUsageUseCase(any()) } returns jsonObj
        mUsageActivityViewModel.mAllPeopleResponse.observeForever {  }
        mUsageActivityViewModel.requestLoginActivityData(mResourceId)
        assert(mUsageActivityViewModel.mAllPeopleResponse.value != null)
        assert(mUsageActivityViewModel.mAllPeopleResponse.value!!.responseRESPONSESTATUS
                == LiveDataWrapper.RESPONSESTATUS.SUCCESS)
        val testResult = mUsageActivityViewModel.mAllPeopleResponse.value as LiveDataWrapper<MobileNetworkUsageRespone>
        assertEquals(testResult.response!!.result?.total.toString(),mTotalValue)
    }
}