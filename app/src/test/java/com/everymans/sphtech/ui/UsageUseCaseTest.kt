package com.everymans.sphtech.ui

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.everymans.sphtech.base.BaseUTTest
import com.everymans.sphtech.di.configureTestAppComponent
import com.everymans.sphtech.repository.UsageRepository
import com.everymans.sphtech.network.UsageAPIService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.test.inject
import org.mockito.Mockito
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class UsageUseCaseTest : BaseUTTest(){

    private lateinit var mUsageUseCase: UsageUseCase
    val mLoginRepo : UsageRepository by inject()
    val mAPIService : UsageAPIService by inject()
    val mockWebServer : MockWebServer by inject()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    val context = Mockito.mock(Context::class.java)
    val mParam = "1"
    val mCount = 87

    val mTotalValue = "59"
    val mResourceId = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"

    @Before
    fun start(){
        super.setUp()
        //Start Koin with required dependencies
        startKoin{ modules(configureTestAppComponent(getMockWebServerUrl()))}
    }

    @Test
    fun test_login_use_case_returns_expected_value()= runBlocking{

        mockNetworkResponseWithFileContent("success_resp_list.json", HttpURLConnection.HTTP_OK)
        mUsageUseCase = UsageUseCase()

        val dataReceived = mUsageUseCase.processUsageUseCase(mParam)

        assertNotNull(dataReceived)
        assertEquals(dataReceived.result?.total.toString(), mTotalValue)
        assertEquals(dataReceived.result?.resourceId, mResourceId)
    }

    @Test
    fun test_login_use_case_returns_unexpected_value()= runBlocking{

            mockNetworkResponseWithFileContent("success_resp_list.json", HttpURLConnection.HTTP_OK)
            mUsageUseCase = UsageUseCase()

            val dataReceived = mUsageUseCase.processUsageUseCase(mParam)

            assertNotNull(dataReceived)
            assertNotEquals(dataReceived.result?.total.toString(), "50")
            assertNotEquals(dataReceived.result?.resourceId, "cgcgu")
        }

}