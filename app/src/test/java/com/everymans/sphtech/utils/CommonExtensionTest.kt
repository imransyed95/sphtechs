package com.everymans.sphtech.utils

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CommonExtensionTest {

    @Mock
    lateinit var array : ArrayList<Double>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun givenUnSortedArrayShouldReturnFalse() {
        Mockito.`when`(array.size).thenReturn(2)
        Mockito.`when`(array[0]).thenReturn(2.0)
        Mockito.`when`(array[1]).thenReturn(1.0)
        Assert.assertFalse(isSorted(array))
    }

    @Test
    fun givenSortedArrayShouldReturnTrue() {
        Mockito.`when`(array.size).thenReturn(2)
        Mockito.`when`(array[0]).thenReturn(1.0)
        Mockito.`when`(array[1]).thenReturn(2.0)
        Assert.assertTrue(isSorted(array))
    }

}