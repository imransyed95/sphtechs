package com.everymans.sphtech.utils

import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.everymans.sphtech.app.MainApp
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NetworkUtilTest {

    val app = getApplicationContext<MainApp>()

    @Test
    fun isNetworkConnectedTest() {
        val isConnected = NetworkUtils.isNetworkConnected(app.applicationContext)
        Assert.assertTrue(isConnected)
    }
}
