package com.everymans.sphtech.utils

import com.everymans.sphtech.ui.UsageActivity
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric.buildActivity
import org.robolectric.RobolectricTestRunner
import org.robolectric.android.controller.ActivityController

@RunWith(RobolectricTestRunner::class)
class NetworkUsageActivityTest {

    private lateinit var controller : ActivityController<UsageActivity>

    @Before
    fun setup(){
        controller = buildActivity(UsageActivity::class.java).setup().create().resume()
    }

    @Test
    @Throws(Exception::class)
    fun shouldNotBeNull() {
        assertNotNull(controller.get())
    }

}