package com.everymans.sphtech.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.everymans.sphtech.di.configureTestAppComponent
import com.everymans.sphtech.base.BaseUTTest
import com.everymans.sphtech.network.UsageAPIService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.startKoin
import org.koin.test.inject
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class UsageRepositoryTest : BaseUTTest(){

    private lateinit var mRepo: UsageRepository
    val mAPIService : UsageAPIService by inject()
    val mockWebServer : MockWebServer by inject()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    val mTotalValue = "59"
    val mResourceId = "a807b7ab-6cad-4aa6-87d0-e283a7353a0f"

    @Before
    fun start(){
        super.setUp()

        startKoin{ modules(configureTestAppComponent(getMockWebServerUrl()))}
    }

    @Test
    fun test_login_repo_retrieves_expected_data() =  runBlocking<Unit>{

        mockNetworkResponseWithFileContent("success_resp_list.json", HttpURLConnection.HTTP_OK)
        mRepo = UsageRepository()

        val dataReceived = mRepo.getLoginData(mResourceId)

        assertNotNull(dataReceived)
        assertEquals(dataReceived.result?.resourceId, mResourceId)
        assertEquals(dataReceived.result?.total, mTotalValue)
    }
}