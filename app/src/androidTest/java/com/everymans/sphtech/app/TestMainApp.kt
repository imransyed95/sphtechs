package com.everymans.sphtech.app

import org.koin.core.module.Module


class TestMainApp : MainApp() {
    override fun provideDependency() = listOf<Module>()
}