package com.everymans.sphtech.di

import okhttp3.mockwebserver.MockWebServer
import org.koin.dsl.module

val MockWebServerInstrumentationTest = module {

    factory {
        MockWebServer()
    }

}